FROM golang:1.11

MAINTAINER Thorben Stangenberg <thorben@stangenberg.ch>

## Do what is needed

## set an environent variable
#ENV name value

## add files
#ADD src dest

ENV GOPATH /go
ENV PATH ${GOPATH}/bin:$PATH

# Install go tools
RUN go get -u github.com/golang/lint/golint \
    && go get -u github.com/google/wire/cmd/wire \
    && go get -u github.com/onsi/ginkgo/ginkgo \
    && go get -u github.com/onsi/gomega/... \
    && go get -u github.com/shiyanhui/hero/hero \
    && go get golang.org/x/tools/cmd/goimports

# Add apt key for LLVM repository
RUN wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add -

# Add LLVM apt repository
RUN echo "deb http://apt.llvm.org/stretch/ llvm-toolchain-stretch-5.0 main" | tee -a /etc/apt/sources.list

# Install clang from LLVM repository
RUN apt-get update && apt-get install -y --no-install-recommends \
    curl \
    git \
    openssl \
    bzr \
    clang-5.0 \
    tree \
    less \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Set Clang as default CC
ENV CC clang-5.0

# Install Google Cloud SDK
RUN cd / \
    && curl https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-243.0.0-linux-x86_64.tar.gz?hl=de --output google-cloud-sdk.tar.gz \
    && tar -xvzf google-cloud-sdk.tar.gz \
    && rm google-cloud-sdk.tar.gz \
    && /google-cloud-sdk/bin/gcloud components install --quiet app-engine-go

ENV PATH /google-cloud-sdk/bin:$PATH

## expose a port
#EXPOSE port

# Clean up when done.
RUN rm -rf /tmp/* \
  && rm -rf /var/cache/apk/*
